# React Proyecto 3 - Cotizador de Seguros
Sistema que cotiza el costo de un seguro de automovil

[Link to Web:](https://hardcore-khorana-045e04.netlify.app/)

## ¿Herramientas utilizadas?
- useEffect
- useState
- Formulario básico
- Emotion styled
- Spinkit


## ¿Cómo funciona?
- Acorde a los datos que se ingresen se le aumenta un porcentaje al precio base del seguro, gracias a Spinkit se puede implementar una animación de carga y con styled se puede agregar código css entre código de JS.


## ¿Dudas?


